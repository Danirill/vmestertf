import 'core-js/features/map';
import 'core-js/features/set';
import React from 'react';
import ReactDOM from 'react-dom';
import connectVK from '@vkontakte/vk-connect';
import App from './App';
//import registerServiceWorker from './sw';

connectVK.send("VKWebAppInit", {});



// Проверяет, поддерживается ли событие на текущей платформе.
/*if (connect.supports("VKWebAppResizeWindow")) {
    connect.send("VKWebAppResizeWindow", {"width": 800, "height": 1000});
}*/

// Если вы хотите, чтобы ваше веб-приложение работало в оффлайне и загружалось быстрее,
// расскомментируйте строку с registerServiceWorker();
// Но не забывайте, что на данный момент у технологии есть достаточно подводных камней
// Подробнее про сервис воркеры можно почитать тут — https://vk.cc/8MHpmT
// registerServiceWorker();

ReactDOM.render(<App />, document.getElementById('root'));
