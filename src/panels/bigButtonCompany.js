import React, { useState } from 'react';
import {Link} from "react-router-dom";
import '../styles/big_button_style.css';

class bigButtonCompany extends React.Component {
    constructor (props) {
      super(props);
    }
    
    render () {
      return (
        <div style={{}}>
            <div class="block">
                <div class="photo" style={{backgroundImage:`url(${this.props.logo})`}}>
            </div>
            <div class="info">
                    <h3>
                        {this.props.company_name}
                    </h3>
                    <h4>
                        {this.props.description}
                    </h4>
            </div>
            

            <button class="mybutton"><span>{this.props.buttontext}</span></button>
            </div>  
        </div>    
      );
    }
}

export default bigButtonCompany;