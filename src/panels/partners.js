import React from 'react';
import PropTypes from 'prop-types';
import { platform, IOS, Root, ModalRoot, PanelHeaderClose, Button } from '@vkontakte/vkui';
import Panel from '@vkontakte/vkui/dist/components/Panel/Panel';
import PanelHeader from '@vkontakte/vkui/dist/components/PanelHeader/PanelHeader';
import HeaderButton from '@vkontakte/vkui/dist/components/HeaderButton/HeaderButton';
import Icon28ChevronBack from '@vkontakte/icons/dist/28/chevron_back';
import Icon24Back from '@vkontakte/icons/dist/24/back';
import Icon24Sort from '@vkontakte/icons/dist/24/sort';
import PanelHeaderButton from '@vkontakte/vkui/dist/components/PanelHeaderButton/PanelHeaderButton';
import ModalPage from '@vkontakte/vkui/dist/components/ModalPage/ModalPage';
import ModalPageHeader from '@vkontakte/vkui/dist/components/ModalPageHeader/ModalPageHeader';
import CompanyCard from './company_card';
import Search from '@vkontakte/vkui/dist/components/Search/Search';
import List from '@vkontakte/vkui/dist/components/List/List';
import TabbarItem from '@vkontakte/vkui/dist/components/TabbarItem/TabbarItem';
import CompanyWall from './company';
import TEST from './company';
import Persik from './Persik';
import View from '@vkontakte/vkui/dist/components/View/View';
import Avatar from '@vkontakte/vkui/dist/components/Avatar/Avatar';
import Cell from '@vkontakte/vkui/dist/components/Cell/Cell';
import Link from '@vkontakte/vkui/dist/components/Link/Link';
import FormLayoutGroup from '@vkontakte/vkui/dist/components/FormLayoutGroup/FormLayoutGroup';
import Checkbox from '@vkontakte/vkui/dist/components/Checkbox/Checkbox';
import Input from '@vkontakte/vkui/dist/components/Input/Input';
import Separator from '@vkontakte/vkui/dist/components/Separator/Separator';
import { filter } from 'bluebird';
import Icon16Verified from '@vkontakte/icons/dist/16/verified';
import Icon16Chevron from '@vkontakte/icons/dist/16/chevron';
import { colors } from 'material-ui/styles';

const axios = require('axios');

let companies = {};
let filters = {};
let companiesList = [];
var filmap = new Map();


axios.post('http://localhost:3000/api/v1/vk-mini-app/getcompanies', {
  })
  .then(response=> {
    companies = response.data.res;
    axios.get('http://localhost:3000/api/v1/vk-mini-app/getfilters', {
    })
    .then(resp=>{
      filters = resp.data.res;
      for(let i=0; i<filters.length;i++){
        filmap.set(filters[i].category_id, {category_name: filters[i].category_name, checked:true});
      }

      for(let i=0; i<companies.length; i++){
        if(/*companies[i].vk_id_group*/true){
          companiesList.push(
            {
              description: filmap.get(companies[i].category_id) ? filmap.get(companies[i].category_id).category_name : "" ,
              name: companies[i].company_name,
              buttonLink: "https://vk.me/club" /*+ companies[i].vk_id_group*/,
              company_id: companies[i].company_id,
              category_id: companies[i].category_id,
              company_marketplace_id:companies[i].company_marketplace_id
            }
          );
        }
      }

      
      
    })
  }) 

class CompanyMap extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      value: '',
      activeView: 'panel1',
      activeModal: null,
    };
    this.flag = true;
    this.handleChange = this.handleChange.bind(this);
    this.handleCheckBoxChange = this.handleCheckBoxChange.bind(this);
    this.openFilters = this.openFilters.bind(this);
    this.modalBack = () => {
      this.setState({value: this.state.value, activeView: 'panel1', activeModal:null});
    };
  }

  createCheckboxes(){
      let filters = [];
      for (var [key, item] of filmap) {
        filters.push(<Checkbox defaultChecked={item.checked} value={key} onChange={this.handleCheckBoxChange}> {item.category_name} </Checkbox>);
      }
      this.setState({
        filters: filters
      }) 
  }

  openFilters(){
    this.createCheckboxes();
    this.setState({
      value: this.state.value, 
      activeView: this.state.activeView, 
      activeModal:"filters",
    })
  }

  handleCheckBoxChange(event) {
    let target = event.target;
    let activeFilter = filmap.get(Number.parseInt(target.value));
    if(target.checked){
      activeFilter.checked = true;
    }else{
      activeFilter.checked = false;
    }
    filmap.set(Number.parseInt(target.value), activeFilter);
    
  }

  handleChange(text) {
    this.setState({value: text, activeView: 'panel1', filtersStates: this.state.filtersStates, activeModal:null});
  }

  get companiesList () {
    const search = this.state.value.toLowerCase();
    var filter = {
      description: search,
      name: search
    };

    return companiesList.filter(function(item) {
      for (var [mapkey, info] of filmap) {
        if(item.category_id == mapkey && !info.checked){
          return false;
        }
      }
      for (var key in filter) {
        if(item[key].toLowerCase().indexOf(filter[key]) > -1){
          return true;
        }
      }
      return false;
    });
  }

  render() {
    return (
      <React.Fragment>
        <PanelHeader
        left={<Button onClick={this.openFilters}><Icon24Sort/></Button>}
        >Наши партнеры
        </PanelHeader>
        <Search value={this.state.value} onChange={this.handleChange} after="Отмена"/>
          {this.companiesList.length > 0 &&
          <div>
            <ModalRoot activeModal={this.state.activeModal}>
            <ModalPage
              id={"filters"}
              onClose={this.modalBack}
              header={
                <ModalPageHeader
                left={<PanelHeaderClose onClick={this.modalBack} />}
                >
                  Фильтры
                </ModalPageHeader>
              }
            >
              <FormLayoutGroup top="Дополнительно">
                {this.state.filters}
              </FormLayoutGroup>
            </ModalPage>
            </ModalRoot>
            <List>
              {this.companiesList.map(card => 
              <Cell
              size="l"
              description={card.description ? card.description : "category_id: " + card.category_id}
              before={<Avatar src={card.avatar ? card.avatar : "https://vk.com/images/camera_a.gif"}/>}
              bottomContent={
                card.company_id ? <div style={{color:"#5180b7", fontSize:"14px"}}>Получить бонусы ></div> : ""
              }
              category_id = {card.category_id}
              onClick = {() => this.props.goWall(card.company_marketplace_id)}
              >
              {card.name}
              
              </Cell>)
              }  
            </List>
            </div>
          } 
      </React.Fragment>  
    );
  }
}

export default CompanyMap;