import React from 'react';
import PropTypes from 'prop-types';
import { platform, IOS, Root, Button, ModalRoot } from '@vkontakte/vkui';
import Panel from '@vkontakte/vkui/dist/components/Panel/Panel';
import PanelHeader from '@vkontakte/vkui/dist/components/PanelHeader/PanelHeader';
import HeaderButton from '@vkontakte/vkui/dist/components/HeaderButton/HeaderButton';
import Icon28ChevronBack from '@vkontakte/icons/dist/28/chevron_back';
import Icon24Back from '@vkontakte/icons/dist/24/back';
import CompanyCard from './company_card';
import Search from '@vkontakte/vkui/dist/components/Search/Search';
import List from '@vkontakte/vkui/dist/components/List/List';
import TabbarItem from '@vkontakte/vkui/dist/components/TabbarItem/TabbarItem';
import CompanyWall from './company';
import TEST from './company';
import Persik from './Persik';
import View from '@vkontakte/vkui/dist/components/View/View';
import Avatar from '@vkontakte/vkui/dist/components/Avatar/Avatar';
import GoogleMap from './GoogleMap';
import Cell from '@vkontakte/vkui/dist/components/Cell/Cell';
import Link from '@vkontakte/vkui/dist/components/Link/Link';
import Separator from '@vkontakte/vkui/dist/components/Separator/Separator';
import Gallery from '@vkontakte/vkui/dist/components/Gallery/Gallery';
import Header from '@vkontakte/vkui/dist/components/Header/Header';
import Group from '@vkontakte/vkui/dist/components/Group/Group';
import { PanelHeaderClose } from '@vkontakte/vkui';
import PanelHeaderButton from '@vkontakte/vkui/dist/components/PanelHeaderButton/PanelHeaderButton';
import Icon24Locate from '@vkontakte/icons/dist/24/locate';
import Partners from './partners';
import ModalCard from '@vkontakte/vkui/dist/components/ModalCard/ModalCard';
import FixedLayout from '@vkontakte/vkui/dist/components/FixedLayout/FixedLayout';


class MapActivity extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
        activeModal: null,
        activePanel: 'map'
      }
    this.place = {};  
    this.openCard = this.openCard.bind(this);  
    this.closeCard = this.closeCard.bind(this);  
    this.setCompany = this.setCompany.bind(this);
  }
  openCard () { this.setState({ activeModal: 'miniCard', activePanel:this.state.activePanel }); console.log("flagged"); console.log(JSON.stringify(this.state))}
  closeCard () { this.setState({ activeModal: null, activePanel:'map' });}
  goWall () { this.setState({ activePanel: 'wall', activeModal:null });}
  setCompany(place) {this.place = place; this.setState({ activeModal: "miniCard", activePanel:'map' });console.log(place)}


  render () {
    return (
        <div>
        <ModalRoot activeModal={this.state.activeModal}>
            <ModalCard
                    id="miniCard"
                    title={this.place.company_name}
                    onClose={() => this.closeCard()}
                    icon={<Avatar mode="app" src="https://vk.com/images/camera_a.gif" size={72} />}
                    header="{header}"
                    caption={this.place.mini_description}
                    actions={[{
                    title: 'Открыть подробную информацию',
                    action: () => {
                        this.goWall();
                    }
                    }
                    ]}
            />
        </ModalRoot>
        <View activePanel={this.state.activePanel}>    
                <Panel id = 'map'>      
                    <PanelHeader>Карта</PanelHeader>
                    <FixedLayout vertical='bottom'>
                        <div style={{ height: '100vh', width: '100%'}}>
                            <GoogleMap setCompany={this.setCompany} openCard={this.openCard}/>
                        </div> 
                    </FixedLayout>
                </Panel>      
                <Panel id="wall">
                  <CompanyCard goBack={this.closeCard} company_marketplace_id={this.place.company_marketplace_id} id="testing"></CompanyCard>
                </Panel>      
        </View>
        </div>
    );
  }
}

export default MapActivity;