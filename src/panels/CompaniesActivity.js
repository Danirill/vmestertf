import React from 'react';
import PropTypes from 'prop-types';
import { platform, IOS, Root, Button } from '@vkontakte/vkui';
import Panel from '@vkontakte/vkui/dist/components/Panel/Panel';
import PanelHeader from '@vkontakte/vkui/dist/components/PanelHeader/PanelHeader';
import HeaderButton from '@vkontakte/vkui/dist/components/HeaderButton/HeaderButton';
import Icon28ChevronBack from '@vkontakte/icons/dist/28/chevron_back';
import Icon24Back from '@vkontakte/icons/dist/24/back';
import CompanyCard from './company_card';
import Search from '@vkontakte/vkui/dist/components/Search/Search';
import List from '@vkontakte/vkui/dist/components/List/List';
import TabbarItem from '@vkontakte/vkui/dist/components/TabbarItem/TabbarItem';
import CompanyWall from './company';
import TEST from './company';
import Persik from './Persik';
import View from '@vkontakte/vkui/dist/components/View/View';
import Avatar from '@vkontakte/vkui/dist/components/Avatar/Avatar';
import Cell from '@vkontakte/vkui/dist/components/Cell/Cell';
import Link from '@vkontakte/vkui/dist/components/Link/Link';
import Separator from '@vkontakte/vkui/dist/components/Separator/Separator';
import Gallery from '@vkontakte/vkui/dist/components/Gallery/Gallery';
import Header from '@vkontakte/vkui/dist/components/Header/Header';
import Group from '@vkontakte/vkui/dist/components/Group/Group';
import { PanelHeaderClose } from '@vkontakte/vkui';
import PanelHeaderButton from '@vkontakte/vkui/dist/components/PanelHeaderButton/PanelHeaderButton';

import Icon24Locate from '@vkontakte/icons/dist/24/locate';


import Partners from './partners';


class CompanyMap extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
        activePanel: 'list'
      }
    this.goWall = this.goWall.bind(this);
    this.goToList = this.goToList.bind(this);
    this.card_marketplace_id = {}
     
  }
  goWall (card) { this.setState({ activePanel: 'wall' }); this.card_marketplace_id = card}
  goToList () { this.setState({ activePanel: 'list' });}


  render () {
    return (
      <View activePanel={this.state.activePanel}>
        <Panel id="list">
       
          <Partners id="list" goWall={this.goWall}/>
        </Panel>
        <Panel id="wall">
          <CompanyCard goBack={this.goToList} company_marketplace_id={this.card_marketplace_id} id="testing"></CompanyCard>
        </Panel>
      </View>
    );
  }
}

export default CompanyMap;