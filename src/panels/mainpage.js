import React from 'react';
import PropTypes from 'prop-types';
import { platform, IOS, Root, Button, ModalRoot } from '@vkontakte/vkui';
import Panel from '@vkontakte/vkui/dist/components/Panel/Panel';
import PanelHeader from '@vkontakte/vkui/dist/components/PanelHeader/PanelHeader';
import HeaderButton from '@vkontakte/vkui/dist/components/HeaderButton/HeaderButton';
import Icon28ChevronBack from '@vkontakte/icons/dist/28/chevron_back';
import Icon24Back from '@vkontakte/icons/dist/24/back';
import CompanyCard from './company_card';
import Search from '@vkontakte/vkui/dist/components/Search/Search';
import List from '@vkontakte/vkui/dist/components/List/List';
import TabbarItem from '@vkontakte/vkui/dist/components/TabbarItem/TabbarItem';
import CompanyWall from './company';
import TEST from './company';
import Persik from './Persik';
import View from '@vkontakte/vkui/dist/components/View/View';
import Avatar from '@vkontakte/vkui/dist/components/Avatar/Avatar';
import GoogleMap from './GoogleMap';
import Cell from '@vkontakte/vkui/dist/components/Cell/Cell';
import Link from '@vkontakte/vkui/dist/components/Link/Link';
import Separator from '@vkontakte/vkui/dist/components/Separator/Separator';
import Gallery from '@vkontakte/vkui/dist/components/Gallery/Gallery';
import Header from '@vkontakte/vkui/dist/components/Header/Header';
import Group from '@vkontakte/vkui/dist/components/Group/Group';
import { PanelHeaderClose } from '@vkontakte/vkui';
import PanelHeaderButton from '@vkontakte/vkui/dist/components/PanelHeaderButton/PanelHeaderButton';
import Icon24Locate from '@vkontakte/icons/dist/24/locate';
import Partners from './partners';
import ModalCard from '@vkontakte/vkui/dist/components/ModalCard/ModalCard';
import FixedLayout from '@vkontakte/vkui/dist/components/FixedLayout/FixedLayout';


class MainPage extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
        activeModal: null,
        activePanel: 'map'
      }
  }
  render () {
    return (
        <div>
        <Group header={<Header mode="secondary">Sticks right</Header>} description="Развлечения" >
              <Gallery
                slideWidth="90%"
                style={{ height: 150 }}
                bullets="dark"
              >
                <div style={{ backgroundColor: 'var(--destructive)' }} />
                <div style={{ backgroundColor: 'var(--button_commerce_background)' }} />
                <div style={{ backgroundColor: 'var(--accent)' }} />
              </Gallery>
            </Group>
            <Group header={<Header mode="secondary">Sticks right</Header>} description="Кофейни">
              <Gallery
                slideWidth="90%"
                style={{ height: 150 }}
                bullets="dark"
              >
                <div style={{ backgroundColor: 'var(--destructive)' }} />
                <div style={{ backgroundColor: 'var(--button_commerce_background)' }} />
                <div style={{ backgroundColor: 'var(--accent)' }} />
              </Gallery>
            </Group>
            <Group header={<Header mode="secondary">Sticks right</Header>} description="Рестораны и Кафе">
              <Gallery
                slideWidth="90%"
                style={{ height: 150 }}
                bullets="dark"
              >
                <div style={{ backgroundColor: 'var(--destructive)' }} />
                <div style={{ backgroundColor: 'var(--button_commerce_background)' }} />
                <div style={{ backgroundColor: 'var(--accent)' }} />
              </Gallery>
            </Group>
        </div>
    );
  }
}

export default MainPage;