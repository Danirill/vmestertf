import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { platform, IOS, PanelHeader, HeaderButton, Panel, Root } from '@vkontakte/vkui';
import Button from '@vkontakte/vkui/dist/components/Button/Button';
import Avatar from '@vkontakte/vkui/dist/components/Avatar/Avatar';
import Cell from '@vkontakte/vkui/dist/components/Cell/Cell';
import Link from '@vkontakte/vkui/dist/components/Link/Link';
import Separator from '@vkontakte/vkui/dist/components/Separator/Separator';
import Gallery from '@vkontakte/vkui/dist/components/Gallery/Gallery';
import Header from '@vkontakte/vkui/dist/components/Header/Header';
import { PanelHeaderClose } from '@vkontakte/vkui';
import Group from '@vkontakte/vkui/dist/components/Group/Group';
import Icon24Back from '@vkontakte/icons/dist/24/back';
import PanelHeaderButton from '@vkontakte/vkui/dist/components/PanelHeaderButton/PanelHeaderButton';
import View from '@vkontakte/vkui/dist/components/View/View';
import Spinner from '@vkontakte/vkui/dist/components/Spinner/Spinner';
import Background1 from '../img/1.jpeg';
import Background2 from '../img/2.jpg';
import Background3 from '../img/3.png';
import MyCard from './bigButtonCompany';
import { max } from 'moment';

const axios = require('axios');


class CompanyCard extends React.Component {
    constructor (props) {
      super(props);
      this.state = {
           company_name:'',
           text:'',
           description:'',
           start_bonus:-1,
           bot_link:null
      }
      this.company_marketplace_id = props.company_marketplace_id;
      this.goBack = props.goBack;  
      this.flag = true;
    }
    componentDidMount () { 
      if(this.flag){
        axios.post("http://localhost:3000/api/v1/vk-mini-app/getcompany",{
            company_marketplace_id: this.company_marketplace_id
        })
        .then(res =>{
          let data = res.data.res;      
          this.flag = false;
          this.setState({
            text:data.description,
            description:data.mini_description,
            company_name: data.company_name,
            start_bonus:data.start_bonus,
            bot_link: data.bot_link
          })
        })
      }
    }
    
    render () {
      return (
        <Root> 
          <PanelHeader left={<PanelHeaderClose onClick={this.goBack} />}>
          Подробно
          </PanelHeader>
          {this.state &&
          <div>
            <MyCard 
            company_name={this.state.company_name} 
            description={this.state.description} 
            buttontext="К боту" 
            logo="https://upload.wikimedia.org/wikipedia/commons/thumb/1/17/Bufotes_oblongus.jpg/275px-Bufotes_oblongus.jpg" 
            start_bonus={this.state.start_bonus}
            bot_link = {this.state.bot_link}/>

            <Group>
              <Gallery
                slideWidth="90%"
                style={{ height: "40%" }}
                bullets="dark"
                align='left'
              >
                <div>
                  <img src="https://cs4.pikabu.ru/post_img/big/2016/05/22/9/1463928608136663525.jpg" alt="" style={{maxWidth: "100%", maxHeight: "100%", display:"block",objectFit:"cover"}}/>
                </div>
                <div>
                  <img src="https://cs9.pikabu.ru/post_img/big/2017/06/06/6/1496739345187895982.jpg" alt="" style={{maxWidth: "100%", maxHeight: "100%", display:"block",objectFit:"cover"}}/>
                </div>
                <div>
                  <img src="https://cs8.pikabu.ru/post_img/big/2017/06/06/6/1496739220147941564.jpg" alt="" style={{maxWidth: "100%", maxHeight: "100%", display:"block",objectFit:"cover"}}/>
                </div>
              </Gallery>
            </Group> 

            <div style={{paddingRight:20, paddingLeft:20, paddingTop: 20, paddingBottom: 10, color: 'grey' }}>
            {this.state.text}
            </div>
            
            </div>  
        }
        </Root> 
      );
    }
}

 export default CompanyCard;