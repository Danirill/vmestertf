import React, { Component, Fragment } from 'react';


import Marker from './GMapComponents/Marker';
import GoogleMapReact from 'google-map-react';

import GoogleMap from './GMapComponents/GoogleMap';
import axios from 'axios';
import connectVK from '@vkontakte/vk-connect';
import { props } from 'bluebird';

// Return map bounds based on list of places
const getMapBounds = (map, maps, places) => {
  const bounds = new maps.LatLngBounds();

  places.forEach((place) => {
    bounds.extend(new maps.LatLng(
      place.geometry.location.lat,
      place.geometry.location.lng,
    ));
  });
  return bounds;
};

// Re-center map when resizing the window
const bindResizeListener = (map, maps, bounds) => {
  maps.event.addDomListenerOnce(map, 'idle', () => {
    maps.event.addDomListener(window, 'resize', () => {
      map.fitBounds(bounds);
    });
  });
};

// Fit map to its bounds after the api is loaded
const apiIsLoaded = (map, maps, places) => {
  // Get bounds by our places
  const bounds = getMapBounds(map, maps, places);
  // Fit map to bounds
  map.fitBounds(bounds);
  // Bind the resize listener
  bindResizeListener(map, maps, bounds);
};

class Main extends Component {
  constructor(props) {
    super(props);
    this.openCard = props.openCard;
    this.setCompany = props.setCompany;
    this.state = {
      places: [],
      myLocation: {
        lat:56.840441,
        lng:60.615943
      }
    };
    //this.uuidv4 = this.uuidv4.bind(this);
  }

  

  componentDidMount() {
    connectVK.sendPromise("VKWebAppGetGeodata", {})
    .then(res =>{
    if(res.type === "VKWebAppGeodataResult"){
        alert(JSON.stringify(res));
        this.setState({
            places:this.state.places, 
            myLocation: {
                lat: Number.parseFloat(res.data.lat),
                lng: Number.parsefloat(res.data.long)
            }
        })
    }
    })

    axios.get('http://localhost:3000/api/v1/vk-mini-app/mapmarkers',{})
    .then(response => {
        let data = response.data.res;
        
        let markers = response.data.res;
        console.log(markers);
        /*for(let i=0; i<data.length;i++){
          let coords = data[i].company_coords.split(',');
          if(coords.length === 2){
            markers.push({
                company_id:             data[i].company_id,
                company_marketplace_id: data[i].company_marketplace_id,
                branch_id:              data[i].company_branch_id, 
                company_name:           data[i].company_name, 
                description:            data[i].description,
                mini_description:       data[i].mini_description,
                category_id:            data[i].category_id, 
                account_phone:          data[i].account_phone,
                company_phone:          data[i].company_phone,
                geometry:{ 
                    location:{
                        lat:Number.parseFloat(coords[0]), 
                        lng:Number.parseFloat(coords[1])
                    }
                } 
            });
          }
        }*/
        this.setState({places: markers, myLocation:this.state.myLocation});
    })
  }

  render() {
    const { places, myLocation } = this.state;
    return (
      <Fragment>
        {places && (
          <GoogleMapReact
            bootstrapURLKeys={{ key: "AIzaSyBCNRLXbA86w6lqAOFm-8Cxj2lnt_mFmIY" }}
            defaultCenter={myLocation}
            defaultZoom={14}
            >
            {places.map(place => (
              <Marker
                key={uuidv4()}
                text={place.company_name}
                lat={place.geometry.location.lat}
                lng={place.geometry.location.lng}
                onClick={()=>{this.setCompany(place)}}
              />
            ))}
        </GoogleMapReact>
        )}
      </Fragment>
    );
  }
}

function uuidv4() {
  return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
    (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
  );
}

export default Main;