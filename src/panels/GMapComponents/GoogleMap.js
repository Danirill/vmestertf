import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import GoogleMapReact from 'google-map-react';

const Wrapper = styled.main`
  width: 100%;
  height: 100%;
`;

const GoogleMap = ({ children, ...props }) => (
    <GoogleMapReact
      bootstrapURLKeys={{
        key: "AIzaSyBCNRLXbA86w6lqAOFm-8Cxj2lnt_mFmIY",
        language: 'ru'
      }}
      {...props}
    >
    {children}
    </GoogleMapReact>
);

GoogleMap.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
  ]),
};

GoogleMap.defaultProps = {
  children: null,
};

export default GoogleMap;
