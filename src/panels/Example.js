import React, { Component } from 'react';import PropTypes from 'prop-types';

import { platform, IOS, FixedLayout, Button } from '@vkontakte/vkui';
import PanelHeaderButton from '@vkontakte/vkui/dist/components/PanelHeaderButton/PanelHeaderButton';
import Panel from '@vkontakte/vkui/dist/components/Panel/Panel';
import PanelHeader from '@vkontakte/vkui/dist/components/PanelHeader/PanelHeader';
import Epic from '@vkontakte/vkui/dist/components/Epic/Epic';
import Tabbar from '@vkontakte/vkui/dist/components/Tabbar/Tabbar';
import TabbarItem from '@vkontakte/vkui/dist/components/TabbarItem/TabbarItem';
import View from '@vkontakte/vkui/dist/components/View/View';
import Avatar from '@vkontakte/vkui/dist/components/Avatar/Avatar';
import Icon28Newsfeed from '@vkontakte/icons/dist/28/newsfeed';
import Icon28Search from '@vkontakte/icons/dist/28/search';
import Icon24Locate from '@vkontakte/icons/dist/24/locate';
import Icon24Sort from '@vkontakte/icons/dist/24/sort';
import ModalRoot from '@vkontakte/vkui/dist/components/ModalRoot/ModalRoot';
import ModalCard from '@vkontakte/vkui/dist/components/ModalCard/ModalCard';
import CellButton from '@vkontakte/vkui/dist/components/CellButton/CellButton';
import Root from '@vkontakte/vkui/dist/components/Root/Root'
import Partners from './partners';
import HeaderButton from '@vkontakte/vkui/dist/components/HeaderButton/HeaderButton';
import Icon28ChevronBack from '@vkontakte/icons/dist/28/chevron_back';
import Icon24Back from '@vkontakte/icons/dist/24/back';
import Icon28More from '@vkontakte/icons/dist/28/more';
import Icon28FireOutline from '@vkontakte/icons/dist/28/fire_outline';
import CompaniesActivity from './CompaniesActivity';
import GoogleMap from './GoogleMap';
import MapActivity from './MapActivity';
import MainPage from './mainpage';
const AnyReactComponent = ({ text }) => <div>{text}</div>;
let center = {
  lat: 59.95,
  lng: 30.33
}

class Example extends React.Component {
    constructor (props) {
      super(props);
  
      this.state = {
        activeStory: 'discover',
        activeModal: null
      };
      this.onStoryChange = this.onStoryChange.bind(this);
    }
  
    onStoryChange (e) {
      this.setState({ activeStory: e.currentTarget.dataset.story })
    }

    setActiveModal(e){
      this.setState({activeStory: this.state.activeStory, activeModal:e})
    }

    render () {
  
      return (   
        <Epic activeStory={this.state.activeStory} tabbar={        
          <Tabbar>
            <TabbarItem
              onClick={this.onStoryChange}
              selected={this.state.activeStory === 'feed'}
              data-story="feed"
              text="Партнеры"
            ><Icon28More/></TabbarItem>
            <TabbarItem
              onClick={this.onStoryChange}
              selected={this.state.activeStory === 'discover'}
              data-story="discover"
              text="Главная"
            ><Icon28FireOutline /></TabbarItem>
            <TabbarItem
              onClick={this.onStoryChange}
              selected={this.state.activeStory === 'more'}
              data-story="more"
              text="Карта"
            ><Icon24Locate /></TabbarItem>
          </Tabbar>
        }>
          <Root id="feed" >
              <CompaniesActivity/>       
          </Root>
          <View id="discover" activePanel="discover">
            <Panel id="discover">
              <PanelHeader>Главная</PanelHeader>
              <MainPage/>
            </Panel>
          </View>
          <Root id="more" >
              <MapActivity/>   
          </Root>
        </Epic>
        
      )
    }
  }
  
export default Example;